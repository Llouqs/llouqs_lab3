import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            ArrayList<String> readStrings = readTheFile("src/templates.txt");
            check(readStrings);
            Scanner scan = new Scanner(System.in);
            System.out.println("FleetBuilder started\nEnter fleet budget:");
            int budget = scan.nextInt();
            TreeMap priceMap = createMap(readStrings);
            System.out.println("Fleet has been generated");
            counting(priceMap, budget);
            writeTheFile(priceMap);
            System.out.println("FleetBuilder stopped. Have a nice day, admiral.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void counting(TreeMap<SpaceShip, Integer> map, int budget) {
        int currentBudget = budget;
        int currentPriceOfShip = map.firstKey().price;
        while (currentBudget - map.lastKey().price >= 0) {
            for (var entry : map.entrySet()) {
                if (currentPriceOfShip == entry.getKey().price) {
                    budget -= currentPriceOfShip;
                    if (budget >= 0) {
                        entry.setValue(entry.getValue() + 1);
                        entry.getKey().changeFinalPrice(currentPriceOfShip);
                    } else {
                        budget += currentPriceOfShip;
                    }
                }
                if (budget - currentPriceOfShip < 0) {
                    currentPriceOfShip = entry.getKey().price;
                }
            }
            currentBudget = budget;
            if (currentBudget - map.lastKey().price < 0) break;
        }
        System.out.println(currentBudget + " credits left");
    }

    public static TreeMap<SpaceShip, Integer> createMap(ArrayList<String> readStrings) {
        TreeMap<SpaceShip, Integer> priceMap = new TreeMap<>((o1, o2) -> {
            if (o2.price - o1.price == 0) {
                return 1;
            } else {
                return o2.price - o1.price;
            }
        });
        for (String str : readStrings) {
            SpaceShip ship = new SpaceShip(str);
            priceMap.put(ship, 0);
        }
        //Сделал вывод отдельно чтобы показать отсортированность
        //for (SpaceShip ship : priceMap.keySet()) {
        //    System.out.println(ship.name + " - price: " + ship.price);
        //}
        return priceMap;
    }

    public static void writeTheFile(TreeMap<SpaceShip, Integer> map) throws IOException {
        FileWriter writer = new FileWriter("src/fleet.txt", false);
        for (var entry : map.entrySet()) {
            String text = entry.getValue() + "x " + entry.getKey().getFormattedString() + "\n";
            writer.write(text);
        }
        writer.flush();
        System.out.println("fleet.txt saved successfully");
    }

    public static ArrayList<String> readTheFile(String path) throws IOException {
        File file = new File(path);
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        ArrayList<String> readStrings = new ArrayList<>();
        while (line != null) {
            readStrings.add(line);
            line = reader.readLine();
        }
        return readStrings;
    }

    public static void check(ArrayList<String> readStrings) throws Exception {
        if (readStrings.size() == 0) {
            throw new Exception("Шаблон отсутствует");
        }
        for (String str : readStrings) {
            String[] tmp = str.split(" ");
            if (tmp.length != 5) {
                throw new Exception("Шаблон не соответствует формату");
            }
        }
    }
}
