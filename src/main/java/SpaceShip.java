public class SpaceShip {
    int price;
    int finalPrice;
    String name;
    String thruster;
    String ftl;
    String reactor;


    SpaceShip(String tmpString) throws NumberFormatException, IllegalArgumentException {
        String[] tmp = tmpString.split(" ");
        name = tmp[0];
        finalPrice = 0;
        try {
            price = Integer.parseInt(tmp[1]);
            eThruster.valueOf(tmp[2]);
            thruster = tmp[2];
            eFtl.valueOf(tmp[3]);
            ftl = tmp[3];
            eReactor.valueOf(tmp[4]);
            reactor = tmp[4];
        } catch (NumberFormatException e) {
            throw new NumberFormatException("\nНеверный формат price: " + tmp[1]);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("\nСреди деталей данного SpaceShip есть неизвестные: " + tmpString);
        }
    }

    void changeFinalPrice(int value) {
        finalPrice += value;
    }

    String getFormattedString() {
        return new String(name + " (" + thruster + " " + ftl + " " + reactor + ") - " + finalPrice);
    }
}

enum eThruster {
    chemical,
    ion,
    plasma
};

enum eFtl {
    warp,
    hyperlane,
    jumpdrive
};

enum eReactor {
    fission,
    fusion,
    antimatter
}

